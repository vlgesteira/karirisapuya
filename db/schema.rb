# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180627051921) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "data_fingerprint"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "documents", force: :cascade do |t|
    t.string "documents"
    t.text "name"
    t.text "subject"
    t.string "period"
    t.string "historical_found"
    t.text "reference_data"
    t.string "ethnicity"
    t.string "location"
    t.text "content"
    t.text "abstract"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "historical_account_file_name"
    t.string "historical_account_content_type"
    t.integer "historical_account_file_size"
    t.datetime "historical_account_updated_at"
  end

  create_table "documents_narratives", id: false, force: :cascade do |t|
    t.integer "document_id"
    t.integer "narrative_id"
    t.index ["document_id"], name: "index_documents_narratives_on_document_id"
    t.index ["narrative_id"], name: "index_documents_narratives_on_narrative_id"
  end

  create_table "ethnicities", force: :cascade do |t|
    t.string "name"
    t.string "location"
    t.text "explanation_pt"
    t.text "explanation_en"
    t.string "sources"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "glossaries", force: :cascade do |t|
    t.string "indigenous_word"
    t.string "ethnicity"
    t.string "meaning_pt"
    t.string "meaning_en"
    t.string "sources"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "narratives", force: :cascade do |t|
    t.string "narratives"
    t.string "name_pt"
    t.string "name_en"
    t.string "content_pt"
    t.string "content_en"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "name", default: "", null: false
    t.string "education", default: ""
    t.string "educational_institution", default: ""
    t.string "research_fields", default: ""
    t.string "sponsor_institution", default: ""
    t.datetime "birthdate"
    t.boolean "superadmin", default: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
