class AddAttachmentHistoricalAccountToDocuments < ActiveRecord::Migration[5.1]
  def self.up
    change_table :documents do |t|
      t.attachment :historical_account
    end
  end

  def self.down
    remove_attachment :documents, :historical_account
  end
end
