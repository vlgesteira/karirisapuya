class CreateEthnicities < ActiveRecord::Migration[5.1]
  def change
    create_table :ethnicities do |t|
      t.string :name
      t.string :location
      t.string :explanation_pt
      t.string :explanation_en
      t.string :sources

      t.timestamps
    end
  end
end
