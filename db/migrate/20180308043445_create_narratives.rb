class CreateNarratives < ActiveRecord::Migration[5.1]
  def change
    create_table :narratives do |t|
      t.string :narratives, :name_pt
      t.string :narratives, :name_en
      t.string :narratives, :content_pt
      t.string :narratives, :content_en

      t.timestamps
    end
  end
end
