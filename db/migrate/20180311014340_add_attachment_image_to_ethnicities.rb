class AddAttachmentImageToEthnicities < ActiveRecord::Migration[5.1]
  def self.up
    change_table :ethnicities do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :ethnicities, :image
  end
end
