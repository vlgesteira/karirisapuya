class CreateDocuments < ActiveRecord::Migration[5.1]
  def change
    create_table :documents do |t|
      t.string :documents, :name
      t.string :documents, :subject
      t.string :documents, :period
      t.string :documents, :historical_found
      t.string :documents, :reference_data
      t.string :documents, :ethnicity
      t.string :documents, :location
      t.text :documents, :content
      t.string :documents, :abstract
      t.string :documents, :description

      t.timestamps
    end
  end
end
