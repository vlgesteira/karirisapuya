class ChangeColumnTypesInDocuments < ActiveRecord::Migration[5.1]
  def change
    change_column :documents, :name, :text
    change_column :documents, :subject, :text
    change_column :documents, :reference_data, :text
    change_column :documents, :description, :text
    change_column :documents, :abstract, :text
  end
end
