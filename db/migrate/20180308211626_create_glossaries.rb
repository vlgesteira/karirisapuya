class CreateGlossaries < ActiveRecord::Migration[5.1]
  def change
    create_table :glossaries do |t|
      t.string :indigenous_word
      t.string :ethnicity
      t.string :meaning_pt
      t.string :meaning_en
      t.string :sources

      t.timestamps
    end
  end
end
