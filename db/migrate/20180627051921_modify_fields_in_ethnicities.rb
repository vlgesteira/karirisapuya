class ModifyFieldsInEthnicities < ActiveRecord::Migration[5.1]
  def change
    change_column :ethnicities, :explanation_pt, :text
    change_column :ethnicities, :explanation_en, :text
  end
end
