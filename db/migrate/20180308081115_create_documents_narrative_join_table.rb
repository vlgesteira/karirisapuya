class CreateDocumentsNarrativeJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_table :documents_narratives, id: false do |t|
      t.integer :document_id
      t.integer :narrative_id
    end

    add_index :documents_narratives, :document_id
    add_index :documents_narratives, :narrative_id
  end
end
