KaririSapuya
================

Web application for historical documents about indigenous people in the Half South of Bahia during the mid 19th century (Aplicação web de documentos históricos sobre os povos indígenas do Baixo Sul da Bahia no século 19).

Configuration
-------------

This application requires:

- Ruby 2.4.0
- Rails 5.1.4
- Elasticsearch 5.0.0
- MySQL 5.7.9

Deploy
---------------

For deploy, run the following commands:
``` cd /var/local  ```

``` git clone https://github.com/gvinicius/karisapuya.git ```

``` cd karisapuya ```

``` sudo apt-get install libmysqlclient-dev ```

``` sudo apt-get install imagemagick ```

``` sudo apt install --target-release jessie-backports \
      openjdk-8-jre-headless \
      ca-certificates-java \
      --assume-yes ```

``` wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add - ```

``` sudo apt-get install apt-transport-https ```

``` echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list ```

``` sudo apt-get update && sudo apt-get install elasticsearch ```

``` sudo systemctl start elasticsearch.service ```

``` sudo service elasticsearch start -i ```

``` gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB ```

``` \curl -sSL https://get.rvm.io | bash -s stable --ruby ```

``` source ~/.bashrc ```

``` rvm install 2.4.0 ```

``` rvm use 2.4.0 ```

``` export RAILS_ENV=production ```

``` export DB_KARIRI_USERNAME=someuser```

``` export DB_KARIRI_PASS=somepass ```

``` export DB_KARIRI_DBNAME=somedbname ```

``` export DB_KARIRI_HOST=somehost ```

``` bundle install ```

``` bundle exec rails assets:precompile RAILS_ENV=production ```

``` bundle exec rails db:create db:migrate RAILS_ENV=production ```

``` bundle exec rails db:seed RAILS_ENV=production ```

``` bundle exec rails RAILS_ENV=production RACK_ENV=production searchkick:reindex:all ```

``` service apache2 restart ```
