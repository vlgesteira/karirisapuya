class Glossary < ApplicationRecord
  scope :by_letter, -> (letter) {
    letter ||= 'a'
    where('indigenous_world like ?', "#{letter}%").order('indigenous_word ASC, id ASC')
  }
  validates :indigenous_word, :meaning_pt, presence: true
  validates :indigenous_word, uniqueness: true

  def meaning
    localized_meaning = self["meaning_#{I18n.locale[0, 2]}".to_sym]
    localized_meaning = meaning_pt if localized_meaning.nil?
    localized_meaning
  end

  def attributes
    super.merge(meaning: meaning)
  end
end
