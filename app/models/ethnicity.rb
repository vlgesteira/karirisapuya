class Ethnicity < ApplicationRecord
  validates :name, :explanation_pt, presence: true
  validates :name, uniqueness: true

  has_attached_file :image,
    :styles => {:thumb => ["60#x60#", :jpg]}
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  def explanation
    localized_explanation = self["explanation_#{I18n.locale[0, 2]}".to_sym]
    localized_explanation = explanation_pt if localized_explanation.nil?
    localized_explanation
  end

  def attributes
    super.merge(explanation: explanation)
  end
end
