class Narrative < ApplicationRecord
  paginates_per 10
  has_and_belongs_to_many :documents
  validates :name_pt, :content_pt, presence: true

  def name
    localized_name = self["name_#{I18n.locale[0, 2]}".to_sym]
    localized_name = name_pt if localized_name.nil?
    localized_name
  end

  def content
    localized_content = self["content_#{I18n.locale[0, 2]}".to_sym]
    localized_content = content_pt if localized_content.nil?
    localized_content
  end

  def attributes
    super.merge(content: content, name: name)
  end
end
