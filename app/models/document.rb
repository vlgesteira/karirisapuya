class Document < ApplicationRecord
  paginates_per 10
  validates :name, :historical_account, presence: true
  has_attached_file :historical_account
  validates_attachment :historical_account
  validates_attachment_size :historical_account, less_than: 10.megabytes
  validates_attachment_content_type :historical_account, content_type: ['application/pdf', 'application/vnd.ms-excel',
                                                                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                                                        'application/msword',
                                                                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                                                        'text/plain']

  before_save :extract_text

  def extract_text
    if historical_account.queued_for_write[:original].present?
      my_file = File.read historical_account.queued_for_write[:original].path
      my_file = my_file
      my_content = Henkei.read :text, my_file
      self.content = my_content[0..14519]
     end
  end

  def self.accessible_by(_ability, _action)
    # Instead of a single simple SQL 'WHERE' clause, imagine this accessible_by method can
    # return a variety of different conditions depending on roles the user has.
  end

  searchkick ignore_above: 256
end
