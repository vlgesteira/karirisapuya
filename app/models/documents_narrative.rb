class DocumentsNarrative < ApplicationRecord
  belongs_to :narrative
  belongs_to :document
end
