class NarrativesController < ApplicationController
  before_action :set_narrative, only: [:show]

  def index
    @narratives = Narrative.order(updated_at: :desc).page(params[:page])
  end

  def show
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_narrative
    @narrative = Narrative.find(params[:id])
  end

  def narrative_params
    params.require(:narrative).permit(:page)
  end
end
