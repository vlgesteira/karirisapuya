class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale

  def default_url_options
    { locale: I18n.locale }
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    @locale_alias = 'PT' if I18n.locale == :"pt-BR"
    @locale_alias = 'EN' if I18n.locale == :en
  end

  def application_params
    params.require(:application).permit(:id, :keywords, :page)
  end

  def after_sign_in_path_for(_resource_or_scope)
    rails_admin_path
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :encrypted_password, :education, :educational_institution, :research_fields, :sponsor_institution, :birthdate])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :email, :encrypted_password, :education, :educational_institution, :research_fields, :sponsor_institution, :birthdate])
  end
end
