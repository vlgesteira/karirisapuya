class DocumentsController < ApplicationController
  def index
    @documents = Document.order(created_at: :desc).page(params[:page])
  end

  def search
    if params[:keywords].present?
      @none = false
      @keywords = params[:keywords].to_s
      @documents = Document.search params[:keywords], fields: [:ethnicity, :name, :subject, :period, :location, :content, :abstract, :description], page: params[:page], per_page: 10, highlight: { fields: { content: { fragment_size: 20 } }, tag: '<strong>' }
      @results = @documents.total_count
    else
      @none = true
      @documents = Document.order(created_at: :desc).page(params[:page])
      @results = @documents.count
    end
  end

  private

  def document_params
    params.require(:document).permit(:id, :page, :keywords)
  end
end
