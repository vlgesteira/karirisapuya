json.extract! glossary, :id, :indigenous_word, :ethnicity, :meaning_pt, :meaning_en, :sources, :created_at, :updated_at
json.url glossary_url(glossary, format: :json)
