json.extract! ethnicity, :id, :name, :location, :explanation_pt, :explanation_en, :sources, :created_at, :updated_at
json.url ethnicity_url(ethnicity, format: :json)
