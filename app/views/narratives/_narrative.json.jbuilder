json.extract! narrative, :id, :created_at, :updated_at
json.url narrative_url(narrative, format: :json)
