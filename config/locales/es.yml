---
es:
  app:
    models:
      evaluation:
        bs_caption_plural: Bs caption plural
      place:
        bs_caption_plural: Bs caption plural
  cancel: Cancel
  create: Create
  date:
    abbr_day_names: '["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"]'
    abbr_month_names: '[nil, "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]'
    day_names: '["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"]'
    formats:
      default: "%d/%m/%Y"
      long: "%d de %B de %Y"
      short: "%d de %B"
    month_names: '[nil, "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]'
    order: "[:day, :month, :year]"
  datetime:
    distance_in_words:
      about_x_hours:
        one: aproximadamente 1 hora
        other: aproximadamente %{count} horas
      about_x_months:
        one: aproximadamente 1 mês
        other: aproximadamente %{count} meses
      about_x_years:
        one: aproximadamente 1 ano
        other: aproximadamente %{count} anos
      almost_x_years:
        one: quase 1 ano
        other: quase %{count} anos
      half_a_minute: meio minuto
      less_than_x_minutes:
        one: menos de um minuto
        other: menos de %{count} minutos
      less_than_x_seconds:
        one: menos de 1 segundo
        other: menos de %{count} segundos
      over_x_years:
        one: mais de 1 ano
        other: mais de %{count} anos
      x_days:
        one: 1 dia
        other: "%{count} dias"
      x_minutes:
        one: 1 minuto
        other: "%{count} minutos"
      x_months:
        one: 1 mês
        other: "%{count} meses"
      x_seconds:
        one: 1 segundo
        other: "%{count} segundos"
      x_years:
        one: 1 ano
        other: "%{count} anos"
    prompts:
      day: Dia
      hour: Hora
      minute: Minuto
      month: Mês
      second: Segundo
      year: Ano
  enums:
    education:
      basic_education: Educação básica
      doctor: Doutor
      master: Mestre
      masters_in_course: Mestrando
      phd_in_course: Doutorando
      undergraduated: Graduado
      undergratuating: Graduando
  errors:
    format: "%{attribute} %{message}"
    messages:
      accepted: deve ser aceito
      blank: não pode ficar em branco
      confirmation: não é igual a %{attribute}
      empty: não pode ficar vazio
      equal_to: deve ser igual a %{count}
      even: deve ser par
      exclusion: não está disponível
      greater_than: deve ser maior que %{count}
      greater_than_or_equal_to: deve ser maior ou igual a %{count}
      inclusion: não está incluído na lista
      invalid: não é válido
      less_than: deve ser menor que %{count}
      less_than_or_equal_to: deve ser menor ou igual a %{count}
      model_invalid: 'A validação falhou: %{errors}'
      not_a_number: não é um número
      not_an_integer: não é um número inteiro
      odd: deve ser ímpar
      other_than: deve ser diferente de %{count}
      present: deve ficar em branco
      required: é obrigatório(a)
      taken: já está em uso
      too_long:
        one: 'é muito longo (máximo: 1 caracter)'
        other: 'é muito longo (máximo: %{count} caracteres)'
      too_short:
        one: 'é muito curto (mínimo: 1 caracter)'
        other: 'é muito curto (mínimo: %{count} caracteres)'
      wrong_length:
        one: não possui o tamanho esperado (1 caracter)
        other: não possui o tamanho esperado (%{count} caracteres)
    template:
      body: 'Por favor, verifique o(s) seguinte(s) campo(s):'
      header:
        one: 'Não foi possível gravar %{model}: 1 erro'
        other: 'Não foi possível gravar %{model}: %{count} erros'
  helpers:
    select:
      prompt: Por favor selecione
    submit:
      create: Criar %{model}
      submit: Salvar %{model}
      update: Atualizar %{model}
  mongoid:
    attributes:
      document:
        content: Conteúdo
        ethnicity: Etnia
        historical_account: Arquivo
        historical_account_content_type: Formato do arquivo
        historical_account_file_name: Nome do arquivo
        historical_account_file_size: Tamanho do arquivo
        historical_account_fingerprint: Assinatura no arquivo
        historical_account_updated_at: Data de carregamento do arquivo
        historical_found: Acervo histórico
        location: Localidade
        name: Nome
        period: Período/Ano
        reference_data: Data de referência
        subject: Assunto
      narrative:
        content_en: Conteúdo em inglês
        content_pt: Conteúdo em português*
        name_en: Título em inglês
        name_pt: Título em português*
      user:
        education: Nível Educacional
        educational_institution: Insituição de Ensino
        email: E-mail
        name: Nome
        password: Senha
        password_confirmation: Senha Confirmação
        research_fields: Área(s) de pesquisa
        set_password: Nova senha
        sponsor_institution: Instituição de Fomento
    models:
      document:
        one: Documento
        other: Documentos
      narrative:
        one: Narrativa
        other: Narrativas
      user:
        one: Usuário
        other: Usuários
  number:
    currency:
      format:
        delimiter: "."
        format: "%u %n"
        precision: '2'
        separator: ","
        significant: Significant
        strip_insignificant_zeros: Strip insignificant zeros
        unit: R$
    format:
      delimiter: "."
      precision: '3'
      separator: ","
      significant: Significant
      strip_insignificant_zeros: Strip insignificant zeros
    human:
      decimal_units:
        format: "%n %u"
        units:
          billion:
            one: bilhão
            other: bilhões
          million:
            one: milhão
            other: milhões
          quadrillion:
            one: quatrilhão
            other: quatrilhões
          thousand: mil
          trillion:
            one: trilhão
            other: trilhões
          unit: Unit
      format:
        delimiter: Delimiter
        precision: '3'
        significant: 'true'
        strip_insignificant_zeros: 'true'
      storage_units:
        format: "%n %u"
        units:
          byte:
            one: Byte
            other: Bytes
          gb: GB
          kb: KB
          mb: MB
          tb: TB
    percentage:
      format:
        delimiter: "."
        format: "%n%"
    precision:
      format:
        delimiter: "."
  saving: Saving
  select_columns: Select columns
  support:
    array:
      last_word_connector: " e "
      two_words_connector: " e "
      words_connector: ", "
  time:
    am: Am
    formats:
      default: "%a, %d de %B de %Y, %H:%M:%S %z"
      long: "%d de %B de %Y, %H:%M"
      short: "%d de %B, %H:%M"
    pm: Pm
  user_name:
    activerecord:
      model:
        avatar: Foto
        document:
          name: Nome
    errors:
      messages:
        record_invalid: 'A validação falhou: %{errors}'
        restrict_dependent_destroy:
          has_many: Não é possível excluir o registro pois existem %{record} dependentes
          has_one: Não é possível excluir o registro pois existe um %{record} dependente
  views:
    abastract: Abastract
    about: Sobre o Projeto
    abstract: Resumo
    admin: Admin
    description: Descrição
    document: Documento
    documents: Documentos
    email: Email
    ethnicity: Etnia
    info: Ajuda
    info_text: " <li> Para pesquisar no site, é necessário inserir um termo (Título, assunto, ano, localidade, etnia ou termo no texto) na área de busca disponível na Home e na página Pesquisar do menu; </li> <li> Expressões com várias palavras são encaradas como um único termo composto; </li> <li> Para baixar o documento, clique no título dele; </li> <li> Se você desejar navegar ao longo dos documentos sem o recurso de busca, utilize a página Documentação Completa no menu.</li> <li> Na seção Narrativas, você pode ler e compartilhar algumas histórias relacionadas com as fontes histórias aqui dispostas, bem como de temas relacionados. </li>"
    location: Localidade
    login: Acessar o sistema de administrador
    narrative: Narrativa
    narratives: Narrativas
    no_document: Sem resultados
    no_documents: Nenhum documento
    no_narratives: Nenhuma narrativa
    password: Senha
    period: Período/Ano
    realization: Realização
    search_go: Pesquisar
    search_info: Título, assunto, ano, localidade, etnia ou termo no texto
    search_menu: Pesquisar
    search_title: Cadastro Digital \"A Trajetória dos índios Kariri-Sapuyá da Pedra Branca, Recôncavo Sul Baiano\"
    subject: Assunto
    support: Apoio
