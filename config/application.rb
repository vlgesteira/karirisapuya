require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Karirisapuya
  class Application < Rails::Application
    ENV['RAILS_ADMIN_THEME'] = 'rollincode'
    config.generators do |g|
      g.test_framework :rspec,
                       fixtures: true,
                       view_specs: false,
                       helper_specs: false,
                       routing_specs: false,
                       controller_specs: false,
                       request_specs: false
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
    end

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    config.api_only = false
    config.i18n.available_locales = ['pt-BR', :en]
    # config.i18n.enforce_available_locales = true
    config.i18n.default_locale = 'pt-BR'
    config.i18n.locale = 'pt-BR'
    :w

    config.time_zone = 'Brasilia' # set default time zone to "Brasilia" (UTC -3)
    config.exceptions_app = routes
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
