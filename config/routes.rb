Rails.application.routes.draw do
  resources :ethnicities
  resources :glossaries
  mount Ckeditor::Engine => '/ckeditor'
  #  resources :documents

  root to: 'visitors#index'
  get 'about', to: 'visitors#about'
  get 'info', to: 'visitors#info'
  get 'search', to: 'documents#search'
  get 'documents', to: 'documents#index'
  resources :narratives

  devise_for :users

  authenticate :user do
    #    resources :documents, only: [:new, :create, :edit, :update, :destroy, :delete]
    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  end

  match '*route_not_found', to: 'visitors#index', via: :all
  match '*internal_server_error', to: 'visitors#index', via: :all
end
