Paperclip::Attachment.default_options.merge!(
    :storage => :google_drive,
    :google_drive_client_secret_path => "#{Rails.root}/config/client_secret.json",
    :google_drive_options => {
      :public_folder_id => '1i8cMcADDCgL6f3VfpstIWgjo_Xc-VnW2',
      :application_name => 'karirisapuya'
    }
)

