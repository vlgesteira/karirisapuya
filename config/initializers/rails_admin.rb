RailsAdmin.config do |config|
  config.parent_controller = 'ApplicationController'

  config.main_app_name = ['Karirisapuya', '']
  ### Popular gems integration

  # config.I18n.default_locale = "pt-BR"

  # == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

    config.included_models = ["User", "Document", "Narrative", "Glossary", "Ethnicity"]

  config.actions do
    dashboard # mandatory
    index # mandatory
    new do
      except %w(Ckeditor::Asset Ckeditor::AttachmentFile Ckeditor::Picture DocumentsNarrative)
    end
    edit do
      except %w(Ckeditor::Asset Ckeditor::AttachmentFile Ckeditor::Picture)
    end
    bulk_delete
    show
    delete

    ## With an audit adapter, you can add:
    # history_index
    # history_show

    config.model Ckeditor::Asset do
      visible { false }
    end
    config.model 'Ckeditor::Asset' do
      visible { false }
    end

    config.model 'Narrative' do
      exclude_fields :id, :creaed_at, :updated_at
      field :name_pt
      field :name_en
      field :content_pt, :ck_editor
      field :content_en, :ck_editor
      field :documents
    end

    config.model 'DocumentsNarrative' do
      visible do
        false
      end
    end

    config.model 'Glossary' do
      exclude_fields :id, :creaed_at, :updated_at
    end

    config.model 'Ethnicity' do
      exclude_fields :id, :creaed_at, :updated_at
      field :explanation_pt, :ck_editor
      field :explanation_en, :ck_editor
      %w(name sources image).each do |field|
        field field.to_sym
      end
    end

    config.model 'User' do
      visible do
        bindings[:controller]._current_user.superadmin
      end
      create do
        include_fields :email
      end

      edit do
        field :email do
          help ' '
          read_only true
        end
        field :education, :enum do
          enum do
            ['Básico', 'Graduação em curso', 'Graduado', 'Mestrando', 'Mestre', 'Doutorando', 'Doutor']
          end
          help ' '
        end
        %w(educational_institution name avatar research_fields sponsor_institution).each do |field|
          field field.to_sym
        end
        field :password do
          label 'Nova senha'
          required true
          help 'Não pode ficar vazia. Mantenha a que está para não alterar sua senha'
        end
      end

      list do
        sort_by :name, :email
        %w(name email avatar education avatar).each do |field|
          field field.to_sym
        end
      end
    end

    config.model 'Document' do
      %w(name content subject location ethnicity period abstract description historical_account).each do |field|
        field field.to_sym
      end
    end
  end
end
