require 'rails_helper'

describe Narrative do
  let(:narrative) do
    Narrative.new
  end

  it '#name' do
    expect(narrative.name).to eq(nil)
  end

  it '#content' do
    expect(narrative.content).to eq(nil)
  end

  it '#attributes' do
    expect(narrative.attributes).to eq('id' => nil, 'narratives' => nil, 'name_pt' => nil, 'name_en' => nil, 'content_pt' => nil, 'content_en' => nil, 'created_at' => nil, 'updated_at' => nil, :content => nil, :name => nil)
  end
end
